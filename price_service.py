import datetime
import logging
import json

from flask import Flask
from flask_cors import *
from flask_restplus import Resource, Api, fields
from alpha_vantage.timeseries import TimeSeries

LOG = logging.getLogger(__name__)

app = Flask(__name__)
api = Api(app)
CORS(app, resources = r'/*')

stock = api.model('stock', {
    'ticker': fields.String(required=True, description='Ticker'),
    })

@api.route('/v1/price/<ticker>')
@api.param('ticker', 'The stock ticker to get a price for')
class Price(Resource):
        
    def get(self, ticker):
        ts = TimeSeries(key='YOUR_API_KEY', output_format='JSON')
        response = ts.get_quote_endpoint(ticker)
        
        return response
        
@api.route('/v1/stock')
class Stock(Resource):

    @api.expect(stock)
    def post(self):
        LOG.error('Received payload:')
        LOG.error(api.payload)

        # store this record in a database

        return {'result': 'success',
                'ticker': api.payload['ticker']}


if __name__ == '__main__':
    app.run(debug=True)
