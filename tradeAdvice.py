import datetime
import logging

from flask import Flask
from flask_restplus import Resource, Api, fields
import pandas_datareader as pdr

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

LOG = logging.getLogger(__name__)

app = Flask(__name__)
api = Api(app)

stock = api.model('stock', {
    'ticker': fields.String(required=True, description='Ticker'),
    })
        

@api.route('/v1/advice/<ticker>')
@api.param('ticker', 'The stock ticker to get a price for')
class Advice(Resource):
    def get(self, ticker):
        try:
            df = pdr.get_data_yahoo(ticker)
            #print(df.head())
        except Exception as ex:
            LOG.error('Error getting data: ' + str(ex))
            LOG.error('Recieved price data:')
        # LOG.error(str(df))

        df['Date'] = df.index

        # 21 days is a good approximation of a single month
        df['30d mavg'] = df['Adj Close'].rolling(window=21).mean()
        df['30d std'] = df['Adj Close'].rolling(window=21).std()
        df['Upper Band'] = df['30d mavg'] + (df['30d std'] * 2)
        df['Lower Band'] = df['30d mavg'] - (df['30d std'] * 2)

        cols = ['30d mavg','Upper Band','Lower Band', 'Adj Close']  
        # LOG.error(df['2020'][cols].tail(1).values)
        upperBand = df['2020']['Upper Band'].tail(1).values[0]
        lowerBand = df['2020']['Lower Band'].tail(1).values[0]
        closePrice = df['2020']['Adj Close'].tail(1).values[0]

        if df['2020']['Adj Close'].tail(1).values[0] > df['2020']['Upper Band'].tail(1).values[0]:
            result = "SELL"
        elif df['2020']['Adj Close'].tail(1).values[0] < df['2020']['Lower Band'].tail(1).values[0]:
            result = "BUY"
        else:
            result = "HOLD"
        
        return {"advice": result, "Close Price": closePrice, "Upper Band": upperBand, "Lower Band": lowerBand}
    


if __name__ == '__main__':
    app.run(debug=True)
